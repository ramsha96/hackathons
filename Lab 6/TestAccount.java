public class TestAccount {
    
    public static void main(String[] args) {
        
        Account myAccount = new Account();

        Account [] arrayOfAccounts = new Account[5];
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

        for(int i=0; i<arrayOfAccounts.length; i++){
            Account act = new Account();
            act.setName(names[i]);
            act.setBalance(amounts[i]);
            arrayOfAccounts[i] = act;

            System.out.println("Name: " + arrayOfAccounts[i].getName() + 
            "Balance: " + arrayOfAccounts[i].getBalance());

            arrayOfAccounts[i].addInterest();

            System.out.println("Name: " + arrayOfAccounts[i].getName() + 
             "Balance: " + arrayOfAccounts[i].getBalance());

        }

         myAccount.setName("Ramsha");
         myAccount.setBalance(100000);

        System.out.println("Name: " + myAccount.getName() + 
         "Balance: " + myAccount.getBalance());

         myAccount.addInterest();

         System.out.println("Name: " + myAccount.getName() + 
         "Balance: " + myAccount.getBalance());

    }

}