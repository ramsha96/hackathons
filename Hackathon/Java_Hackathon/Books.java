package Java_Hackathon;

import java.util.HashMap;
import java.util.Map;

public class Books extends Library{

    private static Map<String, Integer> books = new HashMap<String, Integer>();
    private String bookName;
    
    public Books(String name) {
        this.bookName = name;

    }

    @Override
    public void add() {
        
        int code = (int) (100000 + Math.random() * 900000);
        books.put(this.bookName, code);

        System.out.println(this.bookName + " added successfully added!");
    }

    @Override
    public void remove(String book) {
        boolean success = false;
        for (Map.Entry<String, Integer> entry : books.entrySet()) {
           
            if(entry.getKey() == book){
                books.remove(book);
            System.out.println("Book: " + book + ", Value: " + entry.getValue() + " removed from library.");
            success = true;
            }

        }

        if(!success){
            System.out.println("Book: " + book + " does not exist in the library.");
        }

    }

    @Override
    public void borrow(String name) {
        boolean success = false;
        for (Map.Entry<String, Integer> entry : books.entrySet()) {
           
            if(entry.getKey() == name){
                books.remove(name);
            System.out.println("Book: " + name + ", Value: " + entry.getValue() + " borrowed from library.");
            success = true;
            break;
            }
        }

        if(!success){
            System.out.println("Book: " + name + " does not exist in the library.");
        }

    }

   
}