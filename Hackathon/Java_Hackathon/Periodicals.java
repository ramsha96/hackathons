package Java_Hackathon;

import java.util.HashMap;
import java.util.Map;

public class Periodicals extends Library {
    
    private static Map<String, Integer> periodicals = new HashMap<String, Integer>();
    private String periodicalName;
    
    public Periodicals(String name) {
        this.periodicalName = name;

    }

    @Override
    public void add() {
        
        int code = (int) (100000 + Math.random() * 900000);
        periodicals.put(this.periodicalName, code);

        System.out.println(this.periodicalName + " added successfully added!");
    }

    @Override
    public void remove(String name) {
        Boolean success = false;
        for (Map.Entry<String, Integer> entry : periodicals.entrySet()) {
           
            if(entry.getKey() == name){
                periodicals.remove(name);
            System.out.println("Periodical: " + name + ", Value: " + entry.getValue() + " removed from library.");
            success = true;
            }
        }

        if(!success){
            System.out.println("Periodical: " + name + " does not exist in the library.");
        }

    }

    @Override
    public void borrow(String name) {
        
        System.out.println("Periodicals cannot be borrowed!");

    }



}